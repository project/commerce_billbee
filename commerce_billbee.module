<?php

use Drupal\entity\BundleFieldDefinition;

/**
 * Creates the $order->billbee_ack field.
 *
 * @param string $order_type_id
 *   The order type ID.
 *
 * @return \Drupal\commerce\BundleFieldDefinition
 *   The field definition.
 */
function commerce_billbee_get_ordertype_field_definition($order_type_id) {
  $field_definition = BundleFieldDefinition::create('boolean')
    ->setTargetEntityTypeId('commerce_order')
    ->setTargetBundle($order_type_id)
    ->setName('billbee_ack')
    ->setLabel('Billbee sync ack')
    ->setDescription(t('Whether Billbee sent ACK state for this order after last order change.'))
    ->setCardinality(1)
    ->setDefaultValue(FALSE);

  return $field_definition;
}

/**
 * Implements hook_commerce_order_type_insert().
 */
function commerce_billbee_commerce_order_type_insert(Drupal\Core\Entity\EntityInterface $entity) {
  // Create billbee_ack field on new order types.
  $field_definition = commerce_billbee_get_ordertype_field_definition($entity->id());

  /** @var \Drupal\commerce\ConfigurableFieldManagerInterface $configurable_field_manager */
  $configurable_field_manager = \Drupal::service('commerce.configurable_field_manager');
  $configurable_field_manager->createField($field_definition);
}

/**
 * Implements hook_commerce_order_presave().
 */
function commerce_billbee_commerce_order_presave(\Drupal\commerce_order\Entity\OrderInterface $order) {
  // Ensure this is not billbee setting order state to ACK.
  $route_name = \Drupal::routeMatch()->getRouteName();
  if ($route_name !== 'commerce_billbee.api_endpoint') {
    // Order might be changed. Set billbee ack flag to FALSE to retrigger Billbee sync.
    $order->billbee_ack = FALSE;
  }
}
